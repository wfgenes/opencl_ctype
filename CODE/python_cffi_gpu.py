import cffi
import random
import numpy as np
import time

# Define the CFFI interface
ffi = cffi.FFI()
ffi.cdef("""
    int main(float* a_in, float* b_in, float* c_in, int imageW, int imageH);
""")

# Load the compiled C library
lib = ffi.dlopen("./main.so")

# Define the input matrices and output matrix in Python
imageW = 128*4
imageH = 128*4
FULL_FILTER = 17

start_time = time.time()

# Generate random float array with imageW*imageH length
image = np.random.rand(imageW * imageH).astype(np.float32)
#image = np.round(image, 6)

print("From python: The first image pixel is" ,image[0], "and the last one is", image[1], "\n")
print(f"{image[0]:.15f}")

# Generate random float array with FULL_FILTER length
filter = np.random.rand(FULL_FILTER).astype(np.float32)
print(f"{filter[0]:.15f}")

#filter =np.round(filter, 6)

print("From python: The first filter pixel is" ,filter[0], "and the last one is", filter[1],"\n",)

c = np.zeros_like(image)

# Define input pointers
a = ffi.cast("float*", image.ctypes.data)
b = ffi.cast("float*", filter.ctypes.data)
c = ffi.cast("float*", c.ctypes.data)

imageW = ffi.cast("int", imageW)
imageH = ffi.cast("int", imageH)


# Call the custom function from Python
lib.main(a, b, c, imageW, imageH)

end_time = time.time()
total_time = end_time - start_time

# Print the result
print("The output is", c)

print("Total CFFI time is", total_time)
