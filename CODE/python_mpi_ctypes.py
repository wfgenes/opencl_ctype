import ctypes
import numpy as np
import random
import time
from mpi4py import MPI
import auxiliary
import pickle

#import multiprocessing as mp

# Define the delayed functions







if __name__ == "__main__":
    # Initialize MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    start_time = MPI.Wtime()
    #Execute different actions based on the rank
    if rank == 0:
        print("Master process is running")
        # Perform actions specific to the master process
    else:
        print(f"Worker process {rank} is running")
        # Perform actions specific to worker processes
        resukt = auxiliary.call_opencl_kernel(rank)
        print("The resullt od rank {} is {}".format(rank, resukt))
        file_name = 'image_rak_'+str(rank)+'.pkl' 
        with open(file_name, 'wb') as file:
            pickle.dump(resukt,file)

    
    #print(result_1.result(), result_2.result())
    #result = dask.compute(result_1, result_2, result_3, result_4)
        
    comm.Barrier()
    end_time = MPI.Wtime()
    if rank == 0:
        total_time = end_time - start_time
        print("Total execution time:", total_time)
    MPI.Finalize()    