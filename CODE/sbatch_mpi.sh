#!/bin/bash
#SBATCH -J mpi_job               # Job name
#SBATCH -o mpi_job.%j.out        # Output file name (%j expands to job ID)
#SBATCH -e mpi_job.%j.err        # Error file name (%j expands to job ID)
#SBATCH --nodes=1                # Number of nodes
#SBATCH --ntasks-per-node=4      # Number of tasks per node
#SBATCH --time=00:30:00          # Wall time limit (hh:mm:ss)
#SBATCH -p accelerated     # Partition name
#SBATCH --gres=gpu:4
# Load required modules
module load mpi/openmpi/4.0

# Print the nodes allocated to the job
srun hostname

# Run MPI job
#mpirun -np 256 ./main
mpiexec -np 4 python python_mpi_ctypes.py
