import ctypes
import numpy as np
import random
import time
import dask
import parsl
from parsl import python_app
from dask.distributed import Client
from parsl.executors import HighThroughputExecutor
from parsl.executors import ThreadPoolExecutor
from parsl.addresses import address_by_hostname
from parsl.channels import LocalChannel
from parsl.providers import SlurmProvider
from parsl.providers import LocalProvider
from parsl.launchers import SrunLauncher
from parsl.config import Config
import auxiliary
#import multiprocessing as mp

# Define the delayed functions










if __name__ == "__main__":
    


    
    start_time = time.time()
    result_1 = auxiliary.call_opencl_kernel(1)

    print("The ressult is {} and the type is {} and the  -2 is  {}".format(result_1, len(result_1), result_1[-2]))
    
    end_time = time.time()
    
    total_time = end_time - start_time
    print("Total compute time is", total_time)
    