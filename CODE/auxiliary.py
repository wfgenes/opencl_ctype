import ctypes
import numpy as np
import random
import time
import dask
import parsl
from parsl import python_app
from dask.distributed import Client
from parsl.executors import HighThroughputExecutor
from parsl.addresses import address_by_hostname
from parsl.channels import LocalChannel
from parsl.providers import LocalProvider
from parsl.config import Config
import auxiliary


def call_main(lib, image, filter, c, imageW, imageH, gpu_id):
    return lib.main(image, filter, c, imageW, imageH, gpu_id)



#@dask.delayed
#@python_app(executors=['frontera_htex'])
def call_opencl_kernel(rank):

    # Load the C library using ctypes
    lib = ctypes.CDLL("./main_ctype.so")
    #parsl.load()
    # Define the argument and return types of the C function
    lib.main.argtypes = [
        np.ctypeslib.ndpointer(dtype=np.float32),
        np.ctypeslib.ndpointer(dtype=np.float32),
        np.ctypeslib.ndpointer(dtype=np.float32),
        ctypes.c_int,
        ctypes.c_int,
        ctypes.c_int
    ]
    lib.main.restype = ctypes.c_int

    # Define the input matrices and output matrix in Python
    imageW = 3072
    imageH = 3072
    FULL_FILTER = 17

    
    #pool = mp.Pool()
    # Generate random float array with imageW*imageH length
    image_1 = np.random.rand(imageW * imageH).astype(np.float32)
    # Generate random float array with FULL_FILTER length
    filter = np.random.rand(FULL_FILTER).astype(np.float32)
    # Create a zero-filled output array with the same shape as input
    c = np.zeros_like(image_1)
   
    # Call the C function with the input and output arrays
    result = call_main(lib, image_1, filter, c, imageW, imageH, rank)




    return c